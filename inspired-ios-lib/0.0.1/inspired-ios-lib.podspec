#
# Be sure to run `pod lib lint inspired-ios-lib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'inspired-ios-lib'
  s.version          = '0.0.1'
  s.summary          = 'inspired-ios-lib.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
use inspired-ios-lib for your ad project
                       DESC

  s.homepage         = 'https://gitlab.com/zzgocc/inspired-ios-lib'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'zzgo' => 'zzgocc@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/zzgocc/inspired-ios-lib', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  #s.source_files = 'inspired-ios-lib/Classes/inspiredSDK.h'
  s.source_files = 'inspired-ios-lib/Classes/**/*.{h,m}'
  s.public_header_files = 'inspired-ios-lib/Classes/**/*.{h}'
  
  # s.resource_bundles = {
  #   'inspired-ios-lib' => ['inspired-ios-lib/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  #s.public_header_files = 'inspired-ios-lib/Classes/inspiredSDK.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
